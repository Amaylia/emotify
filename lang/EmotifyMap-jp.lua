
--[[ This is the default mapping of emotes to keywords ]]--

--[[ BIG THANKS go to the Tajin who developed the original list! ]]--

-- List of relevant chat channels maps.
local emote   = CHAT_CHANNEL_EMOTE
local say     = CHAT_CHANNEL_SAY
local yell    = CHAT_CHANNEL_YELL
local whisper = CHAT_CHANNEL_WHISPER
local party   = CHAT_CHANNEL_PARTY
local zone    = CHAT_CHANNEL_ZONE
local guild1  = CHAT_CHANNEL_GUILD_1
local guild2  = CHAT_CHANNEL_GUILD_2
local guild3  = CHAT_CHANNEL_GUILD_3
local guild4  = CHAT_CHANNEL_GUILD_4
local guild5  = CHAT_CHANNEL_GUILD_5


--[[ Example Mapping

  {
    ["command"] = "/emoteCommand",
    ["keywords"] = {'word1', 'word2'},
    ["channels"] = {say, party, emote},
  },

-- command: The emote command to trigger or a list of commands to choose at random.
-- keywords: A list of words that when matched, trigger an emote command. ***
-- channels: A list of channels to allow matching within for the above keywords.

*** Warning ***
  Keywords which are used multiple times will overwrite emotes which were
  bound to that keyword previously.
]]

EmotifyMap = {
-- EXAMPLE:
--    {
--        ["command"] = "/emoteCommand",
--        ["keywords"] = {'word1', 'word2'},
--        ["channels"] = {say, party, emote},
--    },

}
