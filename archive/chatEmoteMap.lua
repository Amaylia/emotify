
--[[ This is the default mapping of emotes to keywords ]]--

--[[ BIG THANKS go to the Tajin who developed the original list! ]]--

-- List of relevant chat channels maps.
local emote   = CHAT_CHANNEL_EMOTE
local say     = CHAT_CHANNEL_SAY
local yell    = CHAT_CHANNEL_YELL
local whisper = CHAT_CHANNEL_WHISPER
local party   = CHAT_CHANNEL_PARTY
local zone    = CHAT_CHANNEL_ZONE
local guild1  = CHAT_CHANNEL_GUILD_1
local guild2  = CHAT_CHANNEL_GUILD_2
local guild3  = CHAT_CHANNEL_GUILD_3
local guild4  = CHAT_CHANNEL_GUILD_4
local guild5  = CHAT_CHANNEL_GUILD_5


--[[ Example Mapping

  {
    ["command"] = "/emoteCommand",
    ["keywords"] = {'word1', 'word2'},
    ["channels"] = {say, party, emote},
  },

-- command: The emote command to trigger or a list of commands to choose at random.
-- keywords: A list of words that when matched, trigger an emote command. ***
-- channels: A list of channels to allow matching within for the above keywords.

*** Warning ***
  Keywords which are used multiple times will overwrite emotes which were
  bound to that keyword previously.
]]

EmotifyMap = {
   ["EN"] = {
      {
         ["name"] = "Laughter",
         ["command"] = {"/lol"},
         ["keywords"] = {'lol', 'haha', 'hehe', 'lmao', 'lmfao', 'rofl', 'roflmao'},
         ["channels"] = {say, emote, party},
      },
      {
         ["name"] = "Laughter",
         ["command"] = {"/lol"},
         ["keywords"] = {"laughs", "chuckles", "chuckles", "giggles", "snickers"},
         ["channels"] = {emote},
      },
      {
         ["name"] = "Music",
         ["command"] = {"/flute", "/drum", "/lute"},
         ["keywords"] = {"music", "song"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Dancing",
         ["command"] = {"/dancebreton", "/dancenord", "/danceimperial", "/dancekhajit", "/dancedunmer", "/dancebosmer", "/dancealtmer", "/danceargonian", "/danceorc", "/danceredguard"},
         ["keywords"] = {"dance", "dances", "dancing"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Praying",
         ["command"] = {"/pray"},
         ["keywords"] = {"prays"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Applause",
         ["command"] = {"/applaud"},
         ["keywords"] = {"applauds", "claps"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Surrender",
         ["command"] = {"/surrender"},
         ["keywords"] = {"surrenders"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Taunting",
         ["command"] = {"/taunt", "/doom"},
         ["keywords"] = {"taunts"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Sitting",
         ["command"] = {"/sit", "/sit2", "/sit3", "/sit4", "/sit5", "/sit6"},
         ["keywords"] = {"sits"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Whispering",
         ["command"] = {"/whisper"},
         ["keywords"] = {"whispers"},
         ["channels"] = {emote, say, party},
      },
   },
   -- End of EN Set.

   ["DE"] = {
      {
         ["name"] = "Laughter",
         ["command"] = {"/lol"},
         ["keywords"] = {'lol', 'haha'},
         ["channels"] = {say, emote, party},
      },
      {
         ["name"] = "Music",
         ["command"] = {"/flute", "/drum", "/lute"},
         ["keywords"] = {"musik"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Dancing",
         ["command"] = {"/dancebreton", "/dancenord", "/danceimperial", "/dancekhajit", "/dancedunmer", "/dancebosmer", "/dancealtmer", "/danceargonian", "/danceorc", "/danceredguard"},
         ["keywords"] = {"tanzt", "tanzen"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Praying",
         ["command"] = {"/pray"},
         ["keywords"] = {"betet"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Applause",
         ["command"] = {"/applaud"},
         ["keywords"] = {"klatscht"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Surrender",
         ["command"] = {"/surrender"},
         ["keywords"] = {"ergibt sich"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Taunting",
         ["command"] = {"/taunt", "/doom"},
         ["keywords"] = {"provoziert"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Sitting",
         ["command"] = {"/sit", "/sit2", "/sit3", "/sit4", "/sit5", "/sit6"},
         ["keywords"] = {"setzt sich"},
         ["channels"] = {emote, say, party},
      },
      {
         ["name"] = "Whispering",
         ["command"] = {"/whisper"},
         ["keywords"] = {"flüstern"},
         ["channels"] = {emote, say, party},
      },
   },
    -- End of DE set.

   ["FR"] = {

   },
}
