

--[[-- Trim leading and trailing space.
  *
  * Source: http://lua-users.org/wiki/StringTrim
  *
  * @param {string} s
  *
  * @return {string}
  *   A string with no leading or trailing spaces.
]]--
function trim(s)
    return s:match'^%s*(.*%S)' or ''
end


--[[-- Convert a string into an array based on a separator.
  *
  * @param {string} s
  * @param {string} sep
  *
  * @return {hash}
]]--
function explode(s, sep)
    local tabl = {}
    local part = ''
    local char = ''
    local len  = s:len()
    for i = 1, len, 1 do
        char = s:sub(i, i)
        if char ~= sep then
            part = part .. char
        else
            table.insert(tabl, trim(part))
            part = ''
        end
    end
    if part ~= nil or part ~= '' then
        table.insert(tabl, trim(part))
    end
    return tabl
end


--[[-- Convert a string to a number
  *
  * @param {string} s
  *
  * @return {number}
]]--
function stringToInt(s)
    return type(s) == "string" and tonumber(s) or 0
end


--~ local stringToInt = function(s)
--~    d("Compare " .. unpack(s))
--~    return 0
--~   return s:byte(2) + s:byte(3)
--~   local int = 0
--~   for i = 2, 4, 1 do
--~      int = int + s:byte(i)
--~   end
--~   return int
--~ end

function SORT_BY_NAME(a, b)
    if a == nil and b == nil then return false end
    if a == nil and b ~= nil then return true end
    return stringToInt(a) < stringToInt(b)
end


--[[-- Counting class.
  *
  * This helps with tracking iterations over tables, basic usage is as follows.
  *
  * For iterating forward:
  * local i = _index, myTable = {}
  * myTable[i.n()] = "foo"
  * myTable[i.n()] = "baz"
  *
  * For iteration in reverse:
  * local i = _index, myTable = {}
  * myTable[i.p()] = "
]]--
local _index = {}
_index.v = 0
_index.reset = function(val)
    if val == nil then val = 0 end
    _index.v = val
end
_index.n = function(mod)
    if mod == nil then mod = 1 end
    _index.v = _index.v + mod
    return _index.v
end
_index.p = function(mod)
    if mod == nil then mod = 1 end
    _index.v = _index.v - mod
    return _index.v
end
