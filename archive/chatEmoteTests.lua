--
-- Includes the utils module
--
local utils = require('libs.utils')
local result = ""

--[[--
  * Helper to check if two tables contain the same data.
  *
  * @param {table} t1
  * @param {table} t2
  *
  * @return {bool}
]]--
local tablesMatch = function(t1, t2)
    if type(t1) ~= "table" then return false end
    if type(t2) ~= "table" then return false end
    for k, v in pairs(t1) do
        if t2[k] ~= v then return false end
    end
    for k, v in pairs(t2) do
        if t1[k] ~= v then return false end
    end
    return true
end


--[[------------------------------------
  *
  * Tests for trim utility.
  *
--]]------------------------------------
if utils.trim("  string  ") == "string" then result = "PASS" else result = "FAIL" end
print("trim Test #1: " .. result)

if utils.trim("  string") == "string" then result = "PASS" else result = "FAIL" end
print("trim Test #2: " .. result)

if utils.trim("string  ") == "string" then result = "PASS" else result = "FAIL" end
print("trim Test #3: " .. result)


--[[------------------------------------
  *
  * Tests for explode utility.
  *
--]]------------------------------------
local testExplode1 = "1 2 3 4 5 6 7 8 9 0"
local testExplode2 = "foo bar baz"
local assertExplode1 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"}
local assertExplode2 = {"foo", "bar", "baz"}

if tablesMatch(utils.explode(testExplode1), assertExplode1) then result = "PASS" else result = "FAIL" end
print("explode test #1: " .. result)
print(utils.explode(testExplode1))

if tablesMatch(utils.explode(testExplode2), assertExplode2) then result = "PASS" else result = "FAIL" end
print("explode test #2: " .. result)
print(utils.explode(testExplode2))



--[[------------------------------------
  *
  * Tests for stringToInt utility.
  *
--]]------------------------------------
local testStringInt1 = "21"
local testStringInt2 = "1562385185"
local testStringInt3 = "162361.1786"
local testStringInt4 = "jagdhskjashg"

if utils.stringToInt(testStringInt1) == 21 then result = "PASS"