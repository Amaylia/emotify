
--[[--
  * Emotify - A continuation and rewrite of AutoEmote.
  *
  * AutoEmote: http://www.esoui.com/downloads/fileinfo.php?id=108#info
  * AutoEmote was written by Tajin.  Big thank you to Tajin for all his work!
  *
  * This addOn is a total rewrite of AutoEmote, the goal is to allow players
  * to perform certain emotes automatically when typing certain keywords or
  * phrases into specified channels.
  *
  * @TODO : Create an in-game configuration section.
  * @TODO : Fill out maps for other languages.
  * @TODO : Create a function that can merge conflicting keywords by merging
  *         their commands and channels lists.
]]--

Emotify = {}

Emotify.Name = "Emotify"
Emotify.Author = "Amaylia"

-- Version information.
Emotify._v = {}
Emotify._v.major = 0
Emotify._v.minor = 5
Emotify._v.fixes = 7
Emotify._v.suffix = "-alpha"
Emotify.Version = string.format("%01d.%01d.%01d%s",
    Emotify._v.major,
    Emotify._v.minor,
    Emotify._v.fixes,
    Emotify._v.suffix
);

Emotify.DisplayName = Emotify.Name .. " v" .. Emotify.Version

-- Emote mappings.
Emotify.emoteList = {}
Emotify.keywordMap = {}
Emotify.phraseMap = {}

-- Settings.
Emotify.Settings = {}
Emotify.Settings.Enabled = true
Emotify.Settings.Debug = false
Emotify.SavedVariableVersion = 3


--[[--
  * Initialize this addOn.
  *
  * Creates a map to link an emote command to an emote id.
  * Creates a map which links a keyword to an emote command.
  * Register with the game to receive chat messages.
]]--
function Emotify:Initialize(EmoteMap)
    Emotify.Player = GetUnitName("player")
    Emotify:InitializeEmoteList()
    Emotify:LoadAliasMaps(EmoteMap)
    -- Disabled until I update libraries.
    --Emotify:InitializeUI()
end

--[[--
  * Create an emote list.
  *
  * Zenimax plays emotes based on numeric values, but these aren't
  * very convenient to use, so we create a table that indexes the
  * integers based on their slash commands.  When we know which slash
  * command we want to play then we call Emotify.emoteList["/dance"]
  * to get the correct code, and then we can PlayEmoteByIndex(integer).
  *
  * @return {table}
  *   An emoteList {["command"] = emote_id, ... etc}.
]]--
function Emotify:InitializeEmoteList()
    local emoteName
    for i = 1, GetNumEmotes() do
        emoteName, _, _ = GetEmoteInfo(i)
        if emoteName ~= nil then
            Emotify.emoteList[emoteName] = i
        end
    end
end

--[[--
  * Create an alias map.
  *
  * Use an emote map (which is loaded from chatEmoteMap.lua) to
  * create alias maps.  The alias maps provides us with a quick way
  * to look up keywords that might be present in a chat message.
  *
  * Whenever a keyword is specified which contains spaces, it is
  * mapped as a phrase belonging to the first keyword in its phrase.
  * Channels are also re-indexed for quick look-ups.
  *
  * Example:
  * -- Original emoteMap (user-friendly) --
  * emoteMap = {
  *    {
  *       ["command"] = {"/lol"},
  *       ["keywords"] = {"lol", "haha", "too funny", "you crack me up"},
  *       ["channels"] = {say, emote, party},
  *    },
  *    {
  *       ["command"] = {"/headscratch"},
  *       ["keywords"] = {"too many variables"},
  *       ["channels"] = {say, emote, party},
  *    },
  *    {
  *       ["command"] = {"/point"},
  *       ["keywords"] = {"you"},
  *       ["channels"] = {say, party},
  *    },
  * }
  *
  * -- Processed emoteMap (code-friendly) --
  * emoteMap = {
  *    [0] = {
  *       ["command"] = {"/lol"},
  *       ["keywords"] = {"lol", "haha", "too funny", "you crack me up"},
  *       ["channels"] = {say, emote, party},
  *       ["channel"] = {[0] = 0, [6] = 6, [3] = 3},
  *    },
  *    [1] = {
  *       ["command"] = {"/headscratch"},
  *       ["keywords"] = {"too many variables"},
  *       ["channels"] = {say, emote, party},
  *       ["channel"] = {[0] = 0, [6] = 6, [3] = 3},
  *    },
  *    [2] = {
  *       ["command"] = {"/point"},
  *       ["keywords"] = {"you"},
  *       ["channels"] = {say, party},
  *       ["channel"] = {[0] = 0, [3] = 3},
  *    },
  * }
  *
  * -- Resulting keywordMap (uses references to emoteMap) --
  * keywordMap = {
  *    ["lol"] = emoteMap[0],
  *    ["haha"] = emoteMap[0],
  *    ["you"] = emoteMap[2],
  * }
  *
  * -- Resulting phraseMap (uses references to emoteMap) --
  * phraseMap = {
  *    ["too"] = {
  *       ["too funny"] = emoteMap[0],
  *       ["too many variables"] = emoteMap[1],
  *    },
  *    ["you"] = {
  *       ["you crack me up"] = emoteMap[0],
  *    },
  * }
  *
  * Examples
  * =====
  * 1. I have far too many variables to consider!        -- plays /headscratch
  * 2. This function is so ridiculous, it is too funny!  -- plays /lol
  * 3. Wow, you crack me up!                             -- plays /lol
  * 4. Hey, you.                                         -- plays /point
  *
  * @param {table} emoteMap
  *   The emote map should be defined in chatEmoteMap.lua.
]]--
function Emotify:LoadAliasMaps(map)
    -- Reset the maps.
    Emotify.keywordMap = {}
    Emotify.phraseMap = {}
    local fragment, index

    for id, emote in pairs(map) do
        -- Re-index channels for quick look-ups.
        if emote.channels ~= nil then
            map[id].channel = {}
            for _, cid in pairs(emote.channels) do
                map[id].channel[cid] = cid
            end
        end

        -- The alias map is indexed by keywords for fast look-ups, the alias
        -- references the entire emote object and all of its data.
        if emote.keywords ~= nil then
            for kid, keyword in pairs(emote.keywords) do
                keyword = string.lower(keyword)
                -- This section deals with keywords which have spaces in them.
                index,_ = string.find(keyword, " ")
                if type(index) == "number" then
                    fragment = string.sub(keyword, 0, (index - 1))
                    if Emotify.phraseMap[fragment] == nil then
                        Emotify.phraseMap[fragment] = {}
                    end
                    Emotify.phraseMap[fragment][keyword] = map[id]
                    --table.insert(Emotify.phraseMap[fragment], {[keyword] = emoteMap[id]})

                -- And this section deals with singular keywords
                elseif keyword ~= nil then
                    Emotify.keywordMap[keyword] = map[id]

                -- Otherwise we cannot load this option.
                else
                    d("invalid configuration option at " .. kid)
                end
            end
        end
    end -- End of for pairs(emoteMap) loop.
end


--[[--
  * Filter a string of certain characters.
  *
  * It is sometimes useful while checking for singular keywords in a string to
  * remove everything except letters, numbers and spaces so that we can split
  * the string down to just its words and check them against our table of known
  * keywords.
  *
  * @param {string} message
  *   A string of characters to be filtered.
  *
  * @return {string}
  *   A string of letters, numbers, and spaces (minus space on front or back).
]]--
function Emotify:filterText(message)
    if (Emotify.Settings.Debug) then
        d("Apply Filter to message: " .. message)
    end
    if string.len(message) == 0 then return message end
    message = string.gsub(message, "[^%w%s]", "")
    if (Emotify.Settings.Debug) then
        d("Filtered message is: " .. trim(message))
    end
    return trim(message)
end

--[[--
  * Maps an emote object to an emote id.
  *
  * If multiple emotes are contained in an emote table, one emote will be
  * chosen at random and returned.  Emote tables should come from the
  * keywordMap table.
  *
  * @see Emotify.LoadAliasMaps
  *
  * @param {table} alias
  *   An emote table with at least the following keys:
  *   - 'command': a list of emote slash commands.
  *
  * @return {number}
  *   An integer representing an emote command.
]]--
function Emotify:GetEmote(alias)
    if alias ~= nil and alias.command ~= nil then
        local command = alias.command
        if command ~= nil then
            local emote = command[math.random(#command)]
            if Emotify.emoteList[emote] ~= nil then
                return Emotify.emoteList[emote]
            end
        end
    end
end

--[[--
  * Check if a player matches the player running this addon.
  *
  * @param {string} player
  *   A Zenimax encoded player name string.
  *
  * @return {boolean}
--]]--
function Emotify:IsPlayerSelf(player)
    return (zo_strformat(SI_UNIT_NAME, player) == Emotify.Player)
end

--[[--
  * Check if this addon is enabled.
  *
  * @return {boolean}
--]]--
function Emotify:IsEnabled()
    return (Emotify.Settings.Enabled == true)
end

--[[--
  * Fix character names sent by chat events.
  *
  * Many names come in as "FirstName LastName^Fx" or with other such suffixes,
  * in order to compare this name with our players name, we need to remove
  * those extra characters from the name.
  *
  * @param {string} name
  *   A character name passed by EVENT_CHAT_MESSAGE_CHANNEL.
  *
  * @return {string}
  *   A character name without the trailing suffix.
]]--
function Emotify:FixPlayerName(name)
    return zo_strformat(SI_UNIT_NAME, name)
end

--[[--
  * Play an emote.
  *
  * An alias can contain one or more slash commands for emotes
  * reference emotes, so we get a valid emote command and then
  * translate it into an emote id which can then be played using the
  * ZOS PlayEmoteByIndex function.
  *
  * @param {table} alias
  *   This is an emote table with the following keys:
  *   - commands: a list of emote slash commands.
  *   - keywords: a list of keywords associated with the commands.
]]--
function Emotify:PlayEmote(alias)
    if (Emotify.Settings.Debug) then
        d("Attempting to play emote: " .. tostring(alias))
    end
    -- Fetches an emote index from the alias map.
    local emote = Emotify:GetEmote(alias)
    if (Emotify.Settings.Debug) then
        d("Got emote ")
        d(emote)
    end
    -- Attempt to play the emote.
    if emote ~= nil then
        if (Emotify.Settings.Debug) then
            d("Playing emote: " .. emote)
        end
        PlayEmoteByIndex(emote)
    end
end

--[[--
  * Determine when we should ignore a message.
  *
  * Criteria for ignoring messages:
  * - Emotify is not enabled.
  * - The message was not sent by our player.
  * - Our player is in combat.
  * - Our player is in motion.
  *
  * @param {string} playerName
  *   This should be a player name.
  *
  * @return {boolean}
  *   Return false when an event should be ignored
]]--
function Emotify:IgnoreChatEvent(playerName)
    if (Emotify.Settings.Debug) then
        d("Checking if " .. Emotify.Name .. " is enabled: " .. tostring(Emotify:IsEnabled()))
        d("Checking if " .. Emotify:FixPlayerName(playerName) .. " is our player: " .. tostring(Emotify:IsPlayerSelf(playerName)))
        d("Checking if we are in combat: " .. tostring(IsUnitInCombat("player")))
        d("Checking if we are moving: " .. tostring(IsPlayerMoving()))
    end
    return not Emotify:IsEnabled()
        or not Emotify:IsPlayerSelf(playerName)
        or IsUnitInCombat("player")
        or IsPlayerMoving()
end

--[[--
  * Event handler for chat messages.
  *
  * This handler receives every message received by or sent to our
  * our player, and as such, it receives a lot of traffic.  We filter
  * a majority of events out by checking if the player name matches
  * our player's name.
  *
  * Criteria for ignoring messages: Emotify.isChatEventValid
  *
  * After that, we break our message down into single words by
  * separating the message at blank spaces.  Then we check each word
  * against our keywordMap, if a match is found, we check the emote
  * object for additional criteria, such as channels, and phrases.
  * Phrases are more expensive to check since they need to use an
  * expression.  When a particular match is found, the emote object
  * attached to it is sent to our Emotify.PlayEmote function for
  * execution.
  *
  * @param {integer} channel
  *   The id of the channel this message was sent to.
  * @param {string} name
  *   The name of the player who sent the message, by default it
  *   contains some strange characters at the end which need to be
  *   trimmed off.
  * @param {string} text
  *   The message text sent by {name} to {channel}.
]]--
function Emotify:OnChatMessageEvent(channel, player, text, isCustomerService)
    if Emotify:IgnoreChatEvent(player) then
        if (Emotify.Settings.Debug) then
            d("Ignoring event from " .. player .. " in channel #" .. channel)
        end
        return
    end

    if (Emotify.Settings.Debug) then
        d("Processing chat event: " .. text .. " for channel #" .. channel)
    end

    text = string.lower(text)

    local alias

    if text == "debug config" then
        table.foreach(Emotify.Settings, d)
    end

    if text == "debug map" then
        for index, entry in pairs(EmotifyMap) do
            d(EmotifyMap[index].command)
            d(EmotifyMap[index].keywords)
        end
    end

    for keyword in string.gmatch(text, "%S+") do
        -- Try to match phrases before single keywords.
        if Emotify.phraseMap[keyword] ~= nil then
            for phrase, alias in pairs(Emotify.phraseMap[keyword]) do
                if alias.channel[channel] ~= nil then
                    if string.find(text, phrase, 1, true) then
                        if Emotify.Settings.Debug then d("Found phrase match for " .. text) end
                        return Emotify:PlayEmote(alias)
                    end
                end
            end
        end

        -- If no phrases were found, a keyword match is tried.
        keyword = Emotify:filterText(keyword)
        if Emotify.keywordMap[keyword] ~= nil then
            alias = Emotify.keywordMap[keyword]
            if alias.channel[channel] ~= nil then
                if Emotify.Settings.Debug then d("Found keyword match for " .. keyword) end
                return Emotify:PlayEmote(alias)
            end
        end
    end
end

--[[--
  * Logging function for debug output.
  *
  * Just to prevent the code from being littered with debug mode checks.
--]]--
function Emotify:DEBUG(message)
    if Emotify.Settings.Debug then
        d(Emotify.Name .. ": " .. message)
    end
end

--[[--
  * Toggles debug mode on or off.
  *
  * @return {void}
--]]--
function Emotify:ToggleDebug()
    Emotify.Settings.Debug = not Emotify.Settings.Debug
    d("Emotify debugging " .. (Emotify.Settings.Debug and "enabled" or "disabled"))
end

--[[--
  * Outputs the addon version to the console.
  *
  * @return {void}
--]]--
function Emotify:ShowVersion()
    d(Emotify.DisplayName)
end

--[[--
  * Outputs information about an emote to the console.
  *
  * @return {void}
--]]--
function Emotify:ShowEmoteInfo(emote)
    command = "/" .. emote
    Emotify:DEBUG("command = " .. command)
    if Emotify.emoteList[command] ~= nil then
        emoteInfo = GetEmoteInfo(Emotify.emoteList[command])
        d(Emotify.Name .. " : " .. command .. "#" .. tostring(Emotify.emoteList[command]))
        d(emoteInfo)
    else
        d(Emotify.Name .. ": emote not found.")
    end
end

--[[--
  * Outputs information about the emotes in the emote map to the console.
  *
  * @return {void}
--]]--
function Emotify:ShowMaps()
    for index, entry in pairs(EmotifyMap) do
        d("[" .. tostring(index) .. "] commands: \"" .. table.concat(EmotifyMap[index].command, '", "') .. "\"")
        d("[" .. tostring(index) .. "] keywords: \"" .. table.concat(EmotifyMap[index].keywords, '", "') .. "\"")
        d("[" .. tostring(index) .. "] channels: \"" .. table.concat(EmotifyMap[index].channels, ', ') .. "\"")
        d("---")
    end
end

--[[--
  * Handler for Emotify's slash commands.
  *
  * @return {void}
--]]--
function Emotify.OnSlashCommand(command)
    -- Explode the string by spaces.
    command = explode(string.lower(command), " ");
    --d(command)
    if (command[1] == "debug") then
        Emotify:ToggleDebug()
    elseif (command[1] == "version") then
        Emotify:ShowVersion()
    elseif (command[1] == "info") then
        Emotify:ShowEmoteInfo(command[2])
    elseif (command[1] == "maps") then
        Emotify:ShowMaps()
    end
end

--[[--
  * Handler for loading the addon.
  *
  * @param {table} event
  *
  * @return {void}
--]]--
local function OnAddonLoaded(event, addonName)
    if (addonName == Emotify.Name) then
        Emotify:Initialize(EmotifyMap)

        -- Register events.
        EVENT_MANAGER:RegisterForEvent(Emotify.Name, EVENT_CHAT_MESSAGE_CHANNEL, Emotify.OnChatMessageEvent)
        EVENT_MANAGER:UnregisterForEvent(Emotify.Name, EVENT_ADD_ON_LOADED)

        SLASH_COMMANDS["/emotify"] = Emotify.OnSlashCommand
    end
end

EVENT_MANAGER:RegisterForEvent(Emotify.Name, EVENT_ADD_ON_LOADED, OnAddonLoaded)
