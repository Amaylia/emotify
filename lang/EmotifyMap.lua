
--[[ This is the default mapping of emotes to keywords ]]--

-- List of relevant chat channels maps.
local emote   = CHAT_CHANNEL_EMOTE
local say     = CHAT_CHANNEL_SAY
local yell    = CHAT_CHANNEL_YELL
local whisper = CHAT_CHANNEL_WHISPER
local party   = CHAT_CHANNEL_PARTY
local zone    = CHAT_CHANNEL_ZONE
local guild1  = CHAT_CHANNEL_GUILD_1
local guild2  = CHAT_CHANNEL_GUILD_2
local guild3  = CHAT_CHANNEL_GUILD_3
local guild4  = CHAT_CHANNEL_GUILD_4
local guild5  = CHAT_CHANNEL_GUILD_5

-- Reverse map for channel numbers.
EmotifyChannels = {
    [emote]   = "emote",
    [say]     = "say",
    [yell]    = "yell",
    [whisper] = "whisper",
    [party]   = "party",
    [zone]    = "zone",
    [guild1] = "guild1",
    [guild2] = "guild2",
    [guild3] = "guild3",
    [guild4] = "guild4",
    [guild5] = "guild5",
}


--[[ Example Mapping

  {
    ["name"] = "My custom emote section",
    ["command"] = "/emoteCommand",
    ["keywords"] = {'word1', 'word2'},
    ["channels"] = {say, party, emote},
  }

-- command: The emote command to trigger or a list of commands to choose at random.
-- keywords: A list of words that when matched, trigger an emote command. ***
-- channels: A list of channels to allow matching within for the above keywords.

*** Warning ***
  Keywords which are used multiple times will overwrite emotes which were
  bound to that keyword previously.
]]

EmotifyMap = {
    {
        ["name"] = "Laughter",
        ["command"] = {"/lol"},
        ["keywords"] = {'lol', 'haha', 'hehe', 'lmao', 'lmfao', 'rofl', 'roflmao'},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Laughter",
        ["command"] = {"/lol"},
        ["keywords"] = {"laughs", "chuckles", "giggles", "snickers"},
        ["channels"] = {emote},
    },
    {
        ["name"] = "Surprised",
        ["command"] = {"/surprised"},
        ["keywords"] = {"Oh wow", "Oh no", "gasp", "gasps", "eep"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Angry",
        ["command"] = {"/angry"},
        ["keywords"] = {"I'm so mad", "irritating", "ugh", "grr", "rawr", "frustrating", "frustrated"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Approve",
        ["command"] = {"/approve"},
        ["keywords"] = {"sure", "okay", "yes", "yep", "yup", "yeah", "I concur", "I agree", "makes sense"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Approve",
        ["command"] = {"/approve"},
        ["keywords"] = {"agrees", "agree"},
        ["channels"] = {emote},
    },
    {
        ["name"] = "No",
        ["command"] = {"/no"},
        ["keywords"] = {"no", "nope", "nuh unh"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Confused",
        ["command"] = {"/confused"},
        ["keywords"] = {"uhmm", "how", "why", "what", "huh", "oookay"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Disapprove",
        ["command"] = {"/disapprove", "/no"},
        ["keywords"] = {"no way", "please, no"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Thankful",
        ["command"] = {"/thanks", "/curtsey"},
        ["keywords"] = {"Thanks", "thank you"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Overwhelmed",
        ["command"] = {"/faint"},
        ["keywords"] = {"faints"},
        ["channels"] = {emote},
    },
    {
        ["name"] = "Congratulations",
        ["command"] = {"/grats"},
        ["keywords"] = {"Congratulations", "congrats", "grats", "gratz"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Gross",
        ["command"] = {"/disgust"},
        ["keywords"] = {"eww", "gross", "yuck", "ick"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Greet",
        ["command"] = {"/greet"},
        ["keywords"] = {"greetings", "hello", "hi", "heya"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Goodbye",
        ["command"] = {"/wave"},
        ["keywords"] = {"good bye", "goodbye", "take care", "see ya", "see you", "good night", "farewell", "fare thee well", "fare well", "cya"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Apologize",
        ["command"] = {"/beg"},
        ["keywords"] = {"sorry", "my bad", "apologies", "forgive me"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Welcome",
        ["command"] = {"/welcome"},
        ["keywords"] = {"welcome"},
        ["channels"] = {say, emote, party},
    },
    {
        ["name"] = "Music",
        ["command"] = {"/flute", "/drum", "/lute"},
        ["keywords"] = {"music", "song"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancebreton", "/dancenord", "/danceimperial", "/dancekhajiit", "/dancedunmer", "/dancebosmer", "/dancealtmer", "/danceargonian", "/danceorc", "/danceredguard"},
        ["keywords"] = {"dance", "dances", "dancing"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancenord"},
        ["keywords"] = {"dance like a nord", "dances like a nord"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/danceimperial"},
        ["keywords"] = {"dance like an imperial", "dances like an imperial", "dances like a maniac", "dances like crazy"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancekhajiit"},
        ["keywords"] = {"dances like a khajiit", "dance like a khajiit", "dances like a kitty", "dance like a kitty"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancedunmer"},
        ["keywords"] = {"dances like an dunmer", "dance like an dunmer", "dances like a dark elf", "dance like a dark elf"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancebosmer"},
        ["keywords"] = {"dances like a bosmer", "dance like a bosmer", "dances like a wood elf", "dance like a wood elf"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancealtmer"},
        ["keywords"] = {"dances like an altmer", "dance like an altmer", "dances like a high elf", "dance like a high elf"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/danceargonian"},
        ["keywords"] = {"dances like an argonian", "dance like an argonian", "dance like a lizard", "dances like a lizard"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/danceorc"},
        ["keywords"] = {"dances like an orc", "dance like an orc"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/danceredguard"},
        ["keywords"] = {"dances like a redguard", "dance like a redguard"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancebreton"},
        ["keywords"] = {"dances like a breton", "dance like a breton"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Dancing",
        ["command"] = {"/dancedrunk"},
        ["keywords"] = {"dances drunkenly"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Praying",
        ["command"] = {"/pray"},
        ["keywords"] = {"prays", "I pray"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Cheering",
        ["command"] = {"/cheer"},
        ["keywords"] = {"yay", "hurray", "wow", "great job", "nice job", "awesome"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Applause",
        ["command"] = {"/applaud"},
        ["keywords"] = {"applauds", "claps", "impressive"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Surrender",
        ["command"] = {"/surrender"},
        ["keywords"] = {"surrenders"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Taunting",
        ["command"] = {"/taunt", "/doom"},
        ["keywords"] = {"taunts", "bring it on", "Prepare yourself", "I challenge"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Sitting",
        ["command"] = {"/sit", "/sit2", "/sit3", "/sit4", "/sit5", "/sit6"},
        ["keywords"] = {"sits"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Whispering",
        ["command"] = {"/whisper"},
        ["keywords"] = {"whispers"},
        ["channels"] = {emote, say, party},
    },
    {
        ["name"] = "Pain",
        ["command"] = {"/headache", "/heartache"},
        ["keywords"] = {"ouch", "owwie", "ouchie", "ow", "owww"},
        ["channels"] = {say, party},
    },
    {
        ["name"] = "Sigh",
        ["command"] = {"/downcast", "/heartache"},
        ["keywords"] = {"ouch", "owwie", "ouchie", "ow", "owww"},
        ["channels"] = {say, party},
    },
}
