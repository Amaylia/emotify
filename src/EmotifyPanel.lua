--[[--
  * Implements LibAddonMenu-2.0
  *
  * @link https://github.com/sirinsidiator/ESO-LibAddonMenu
]]--

Emotify = Emotify or {}

local EmotifyPanel = {
    type = "panel",
    name = "Emotify",
}

local EmotifyPanelOptions = {
    [1] = {
        type = "checkbox",
        name = "My Checkbox",
        tooltip = "Checkbox's tooltip text.",
        getFunc = function() return true end,
        setFunc = function(value) d(value) end,
    },
    [2] = {
        type = "dropdown",
        name = "My Dropdown",
        tooltip = "Dropdown's tooltip text.",
        choices = {"table", "of", "choices"},
        getFunc = function() return "of" end,
        setFunc = function(var) print(var) end,
    },
    [3] = {
        type = "slider",
        name = "My Slider",
        tooltip = "Slider's tooltip text.",
        min = 0,
        max = 20,
        getFunc = function() return 3 end,
        setFunc = function(value) d(value) end,
    },
}

function Emotify:InitializeUI()
    local LAM2 = LibStub("LibAddonMenu-2.0")
    LAM2:RegisterAddonPanel("EmotifyOptions", EmotifyPanel)
    LAM2:RegisterOptionControls("EmotifyOptions", EmotifyPanelOptions)
end
