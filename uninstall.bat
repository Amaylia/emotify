@echo off

SET EMOTIFY_DIR=%HOMEPATH%\Documents\Elder Scrolls Online\live\Addons\Emotify

echo =====================
echo Uninstalling Emotify.
echo =====================

rmdir /s /q "%EMOTIFY_DIR%"

echo Done.
echo.

