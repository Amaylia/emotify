@echo off

REM Emotify addon directory.
SET EMOTIFY_DIR=%HOMEPATH%\Documents\Elder Scrolls Online\live\Addons\Emotify


REM Uninstall previous versions.
REM ============================
IF EXIST "%EMOTIFY_DIR%" (
    uninstall.bat
    install.bat
)


REM Emotify subfolders.
REM ===================
SET EMOTIFY_SRC=%EMOTIFY_DIR%\src
SET EMOTIFY_LIB=%EMOTIFY_DIR%\lib
SET EMOTIFY_LANG=%EMOTIFY_DIR%\lang


echo ===================
echo Installing Emotify.
echo ===================


REM Copy main addon files:
REM ======================
mkdir "%EMOTIFY_DIR%"
mkdir "%EMOTIFY_SRC%"
mkdir "%EMOTIFY_LIB%"
mkdir "%EMOTIFY_LANG%"
copy Emotify.txt "%EMOTIFY_DIR%\"
copy src "%EMOTIFY_SRC%"
copy lang "%EMOTIFY_LANG%"


REM Copy embedded libraries:
REM ========================
REM mkdir "%EMOTIFY_LIB%\LibAddonMenu-2.0"
REM mkdir "%EMOTIFY_LIB%\LibAddonMenu-2.0\controls"
mkdir "%EMOTIFY_LIB%\LibStub"
REM copy lib\LibAddonMenu-2.0 "%EMOTIFY_LIB%\LibAddonMenu-2.0"
REM copy lib\LibAddonMenu-2.0\controls "%EMOTIFY_LIB%\LibAddonMenu-2.0\controls"
copy lib\LibStub "%EMOTIFY_LIB%\LibStub"


echo ===============================
echo Emotify Successfully Installed.
echo ===============================
echo.

PAUSE
