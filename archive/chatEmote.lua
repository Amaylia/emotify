
--[[-- chatEmotes - A continuation of AutoEmote.
  *
  * AutoEmote: http://www.esoui.com/downloads/fileinfo.php?id=108#info
  * AutoEmote was written by Tajin.  Big thank you to Tajin for all his work!
  *
  *
  * This addOn is a total rewrite of AutoEmote which seems to have been
  * abandoned quite some time ago, the goal is to allow players to perform
  * certain emotes automatically when typing certain keywords into specified
  * channels.
  *
  * A big thanks to Wykkyd who has indirectly been helping me learn LUA!
  *
  * @TODO : Create an in-game configuration section.
  * @TODO : Allow default emote for a channel.
  * @TODO : Track number of time actions are used.
  * @TODO : Add toggle feature for debugging messages.
  * @TODO : Add UI support for German and French languages.
  * @TODO : Create a function that can merge conflicting keywords by merging
  *         their commands and channels lists.
]]--


local Emotify = {}
Emotify.__index = Emotify

function Emotify.new(init)
    local self = setmetatable({}, Emotify)
    self.Name = "Emotify"
    self.DisplayName = "Emotify"
    self.Author = "Amaylia"
    self.SavedVariableVersion = 3
    self.version = {}
    self.version.major = 0
    self.version.minor = 5
    self.version.fixes = 0

    return self
end

--[[** Name information **]]--
Emotify.Name         = "chatEmote"
Emotify.DisplayName  = "Chat Emote"
Emotify.Author       = "Amaylia"


--[[** General settings **]]--
Emotify.SavedVariableVersion = 3


--[[** Version information **]]--
Emotify._v = {}
Emotify._v.major   = 0
Emotify._v.minor   = 3
Emotify._v.fixes   = 0
Emotify.MAJOR      = Emotify.Name .. "." .. Emotify._v.major
Emotify.MINOR      = string.format(".%02d%03d", Emotify._v.minor, Emotify._v.fixes)
Emotify.Version    = Emotify._v.major
          .. "." .. Emotify._v.minor
          .. "." .. Emotify._v.fixes


--[[** Imported settings (from LibWykkydFactory) **]]--
-- We need to pass the final parameter, our global saved variable as a string.
Emotify.Player = ""
Emotify.Settings = {}
Emotify.GlobalSettings = {}

if chatEmoteGlobal == nil then chatEmoteGlobal = {} end



--[[** AddOn properties **]]--
Emotify.emoteList = {}
Emotify.keywordMap = {}
Emotify.phraseMap = {}
Emotify.languages = {'EN', 'DE', 'FR'}


--[[-- Initialize this addOn.
  *
  * The hard work is handled by Wykkyd's lovely library!
  * Creates a map to link an emote command to an emote id.
  * Creates a map which links a keyword to an emote command.
  * Register with the game to receive chat messages.
]]--
Emotify.Initialize = function(self)
   Emotify.emoteList = Emotify.GetEmoteList()
   Emotify.LoadAliasMaps(chatEmoteMap, self.Settings.Language) -- defined in chatEmoteMap.lua

   self:RegisterEvent(EVENT_CHAT_MESSAGE_CHANNEL, Emotify.onChatMessageEvent, false)
end

--[[-- Create settings menu.
  *
  * TODO: big item still for figuring out how to go about configurations!
  *
  * The below function is a reference to LWF:
  * MakeStandardOption(settingObj, label, settingKey, default, typeOf, extras)
]]--
Emotify.LoadSettingsMenu = function(self)
   local panelData = self:MakeStandardSettingsPanel(Emotify.Author, "|cFF33CC" )
   local optionsTable = {}
   local index = _index

   -- The top level description.
   optionsTable[index.n()] = {
      type = "description",
      text = "This addOn allows you to automatically perform emotes based"
          .. " on the message you type. For example, typing lol into say,"
          .. " emote, or party chat will cause your character to laugh."
   }
   -- For managing profiles between characters.
   optionsTable = self:InjectAdvancedSettings(optionsTable, index.n()-1)
   -- Enable/Disable toggle.
   optionsTable[index.n()] = {
      type = "checkbox",
      name = "Enable Chat Emote",
      tooltip = "Disables or Enables keyword processing.",
      getFunc = function() return self.Settings.Enabled end,
      setFunc = function(val) self.Settings.Enabled = val end,
   }
   -- The language option.
   optionsTable[index.n()] = self:MakeStandardOption(
      self.Settings,
      "Language",
      "Language",
      "EN",
      "dropdown",
      { choices=self._languages, default="" }
   );

   -- Build the settings menu items.
   for i = 1, #self.Settings.Emote, 1 do

   end

--   for command, code in pairs(emoteList) do
--      optionsTable[index.n()] = {
--         type = "submenu",
--         name = command,
--         controls = {
--            [1] = self:MakeStandardOption(self.Settings.Emote[command], "Keywords", "keywords", true, "editbox"),
--            [2] = self:MakeStandardOption(self.Settings.Emote[command].channels, "Say", CHAT_CHANNEL_SAY, true, "checkbox", {default=true}),
--            [3] = self:MakeStandardOption(self.Settings.Emote[command].channels, "Emote", CHAT_CHANNEL_EMOTE, true, "checkbox", {default=true}),
--            [4] = self:MakeStandardOption(self.Settings.Emote[command].channels, "Party", CHAT_CHANNEL_PARTY, true, "checkbox", {default=true}),
--            [5] = self:MakeStandardOption(self.Settings.Emote[command].channels, "Yell", CHAT_CHANNEL_YELL, false, "checkbox", {default=false}),
--         },
--      }
--   end

   self.LAM:RegisterAddonPanel(Emotify.Name .. "_LAM", panelData)
   self.LAM:RegisterOptionControls(Emotify.Name .. "_LAM", optionsTable)
end

--[[-- Initialize saved variables.
  *
  * Here we provide default values for saved variables.
]]--
Emotify.LoadSavedVariables = function(self)
   if self.Settings.Enabled == nil then self.Settings.Enabled = true end
   if self.Settings.Language == nil then self.Settings.Language = "EN" end
   if self.Settings.Emote == nil then self.Settings.Emote = chatEmoteMap end
end

--[[--
  *
  *
  *
]]--
Emotify.addConfigEntry = function(eName, e)
   local entry = {
      type = "checkbox",
      name = "Enable Chat Emote",
      tooltip = "Disables or Enables keyword processing.",
      getFunc = function() return self.Settings.Enabled end,
      setFunc = function(val) self.Settings.Enabled = val end,
   }
   return entry
end

--[[-- Filter a string of certain characters.
  *
  * It is sometimes useful while checking for singular keywords in a string to
  * remove everything except letters, numbers and spaces so that we can split
  * the string down to just its words and check them against our table of known
  * keywords.
  *
  * @param {string} message
  *   A string of characters to be filtered.
  *
  * @return {string}
  *   A string of letters, numbers, and spaces (minus space on front or back).
]]--
Emotify.filterText = function(message)
   if string.len(message) == 0 then return message end
   message = string.gsub(message, "[^%w%s]", "")
   return trim(message)
end

--[[-- Maps an emote object to an emote id.
  *
  * If multiple emotes are contained in an emote table, one emote will be
  * chosen at random and returned.  Emote tables should come from the
  * keywordMap table.
  *
  * @see _addon.LoadAliasMaps
  *
  * @param {table} alias
  *   An emote table with at least the following keys:
  *   - 'command': a list of emote slash commands.
  *
  * @return {number}
  *   An integer representing an emote command.
]]--
Emotify.getEmote = function(alias)
   if alias ~= nil and alias.command ~= nil then
      local command = alias.command
      if command ~= nil then
         local emote = command[math.random(#command)]
         if Emotify.emoteList[emote] ~= nil then
            return Emotify.emoteList[emote]
         end
      end
   end
end

--[[-- Fix character names sent by chat events.
  *
  * Many names come in as "FirstName LastName^Fx" or with other such suffixes,
  * in order to compare this name with our players name, we need to remove
  * those extra characters from the name.
  *
  * @param {string} name
  *   A character name passed by EVENT_CHAT_MESSAGE_CHANNEL.
  *
  * @return {string}
  *   A character name without the trailing suffix.
]]--
Emotify.fixPlayerName = function(name)
   if name == nil then return "" end
   local i = string.find(name, "^", 0, true)
   if i ~= nil then
      name = string.sub(name, 0, (i - 1))
   end
   return name
end

--[[-- Create an emote list.
  *
  * Zenimax plays emotes based on numeric values, but these aren't
  * very convenient to use, so we create a table that indexes the
  * integers based on their slash commands.  When we know which slash
  * command we want to play then we call _addon.emoteList["/dance"]
  * to get the correct code, and then we can PlayEmote(integer).
  *
  * @return {table}
  *   An emoteList {["command"] = emote_id, ... etc}.
]]--
function Emotify:GetEmoteList()
   local emoteName
   local emoteList = {}
   for i = 0, GetNumEmotes() do
      emoteName = GetEmoteSlashName(i)
      if emoteName ~= nil then
         emoteList[emoteName] = i
      end
   end
   return emoteList
end

--[[-- Create an alias map.
  *
  * Use an emote map (which is loaded from chatEmoteMap.lua) to
  * create alias maps.  The alias maps provides us with a quick way
  * to look up keywords that might be present in a chat message.
  *
  * Whenever a keyword is specified which contains spaces, it is
  * mapped as a phrase belonging to the first keyword in its phrase.
  * Channels are also re-indexed for quick look-ups.
  *
  * Example:
  * -- Original emoteMap (user-friendly) --
  * emoteMap = {
  *    {
  *       ["command"] = {"/lol"},
  *       ["keywords"] = {"lol", "haha", "too funny", "you crack me up"},
  *       ["channels"] = {say, emote, party},
  *    },
  *    {
  *       ["command"] = {"/headscratch"},
  *       ["keywords"] = {"too many variables"},
  *       ["channels"] = {say, emote, party},
  *    },
  *    {
  *       ["command"] = {"/point"},
  *       ["keywords"] = {"you"},
  *       ["channels"] = {say, party},
  *    },
  * }
  *
  * -- Processed emoteMap (code-friendly) --
  * emoteMap = {
  *    [0] = {
  *       ["command"] = {"/lol"},
  *       ["keywords"] = {"lol", "haha", "too funny", "you crack me up"},
  *       ["channels"] = {say, emote, party},
  *       ["channel"] = {[0] = 0, [6] = 6, [3] = 3},
  *    },
  *    [1] = {
  *       ["command"] = {"/headscratch"},
  *       ["keywords"] = {"too many variables"},
  *       ["channels"] = {say, emote, party},
  *       ["channel"] = {[0] = 0, [6] = 6, [3] = 3},
  *    },
  *    [2] = {
  *       ["command"] = {"/point"},
  *       ["keywords"] = {"you"},
  *       ["channels"] = {say, party},
  *       ["channel"] = {[0] = 0, [3] = 3},
  *    },
  * }
  *
  * -- Resulting keywordMap (uses references to emoteMap) --
  * keywordMap = {
  *    ["lol"] = emoteMap[0],
  *    ["haha"] = emoteMap[0],
  *    ["you"] = emoteMap[2],
  * }
  *
  * -- Resulting phraseMap (uses references to emoteMap) --
  * phraseMap = {
  *    ["too"] = {
  *       ["too funny"] = emoteMap[0],
  *       ["too many variables"] = emoteMap[1],
  *    },
  *    ["you"] = {
  *       ["you crack me up"] = emoteMap[0],
  *    },
  * }
  *
  * Examples
  * =====
  * 1. I have far too many variables to consider!        -- plays /headscratch
  * 2. This function is so ridiculous, it is too funny!  -- plays /lol
  * 3. Wow, you crack me up!                             -- plays /lol
  * 4. Hey, you.                                         -- plays /point
  *
  * @param {table} emoteMap
  *   The emote map should be defined in chatEmoteMap.lua.
]]--
Emotify.LoadAliasMaps = function(emoteMap, language)
   -- Reset the maps.
   Emotify.keywordMap = {}
   Emotify.phraseMap = {}
   local fragment, index
   local map = emoteMap[language]

   for id, emote in pairs(map) do
      -- Re-index channels for quick look-ups.
      if emote.channels ~= nil then
         map[id].channel = {}
         for _, cid in pairs(emote.channels) do
            map[id].channel[cid] = cid
         end
      end

      -- The alias map is indexed by keywords for fast lookups, the alias
      -- references the entire emote object and all of its data.
      if emote.keywords ~= nil then
         for kid, keyword in pairs(emote.keywords) do
            -- This section deals with keywords which have spaces in them.
            index,_ = string.find(keyword, " ")
            if type(index) == "number" then
               fragment = string.sub(keyword, 0, (index - 1))
               if Emotify.phraseMap[fragment] == nil then
                  Emotify.phraseMap[fragment] = {}
               end
               Emotify.phraseMap[fragment][keyword] = map[id]
               --table.insert(_addon.phraseMap[fragment], {[keyword] = emoteMap[id]})

            -- And this section deals with singular keywords
            elseif keyword ~= nil then
               Emotify.keywordMap[keyword] = map[id]

            -- Otherwise we cannot load this option.
            else
               d("invalid configuration option at " .. kid)
            end
         end
      end
   end -- End of for pairs(emoteMap) loop.
end

--[[-- Play an emote.
  *
  * An alias can contain one or more slash commands for emotes
  * reference emotes, so we get a valid emote command and then
  * translate it into an emote id which can then be played using the
  * ZOS PlayEmote function.
  *
  * @param {table} alias
  *   This is an emote table with the following keys:
  *   - commands: a list of emote slash commands.
  *   - keywords: a list of keywords associated with the commands.
]]--
Emotify.PlayEmote = function(alias)
   local emote = Emotify.getEmote(alias)
   if emote ~= nil then
      PlayEmote(emote)
   end
end

--[[-- Determine when we should ignore a message.
  *
  * Criteria for ignoring messages:
  * - Our addOn is disabled.
  * - The message was not sent by our player.
  * - Our player is in combat.
  * - Our player is moving.
  *
  * @param {string} name
  *   This should be a player name.
  *
  * @return {boolean}
  *   Return false when an event should be ignored
]]--
Emotify.isChatEventValid = function(name)
   if Emotify.Settings.Enabled ~= true             then return false end
   if Emotify.fixPlayerName(name) ~= Emotify.Player then return false end
   if IsUnitInCombat("player")                    then return false end
   if IsPlayerMoving()                            then return false end
   return true
end

--[[-- Event handler for chat messages.
  *
  * This handler receives every message received by or sent to our
  * our player, and as such, it receives a lot of traffic.  We filter
  * a majority of events out by checking if the player name matches
  * our player's name.
  *
  * Criteria for ignoring messages: _addon.isChatEventValid
  *
  * After that, we break our message down into single words by
  * separating the message at blank spaces.  Then we check each word
  * against our keywordMap, if a match is found, we check the emote
  * object for additional criteria, such as channels, and phrases.
  * Phrases are more expensive to check since they need to use an
  * expression.  When a particular match is found, the emote object
  * attached to it is sent to our _addon.PlayEmote function for
  * execution.
  *
  * @param {integer} id
  *   I'm not really sure what this is... maybe it is an event id?
  * @param {integer} channel
  *   The id of the channel this message was sent to.
  * @param {string} name
  *   The name of the player who sent the message, by default it
  *   contains from strange characters at the end which need to be
  *   trimmed off.
  * @param {string} text
  *   The message text sent by {name} to {channel}.
]]--
Emotify.onChatMessageEvent = function(id, channel, name, text)
   if Emotify.isChatEventValid(name) ~= true then return end

   local alias

   if text == "debug config" then
      table.foreach(Emotify.Settings.Emote, d)
   end

   if text == "debug map" then
      for index, entry in pairs(chatEmoteMap) do
         d(chatEmoteMap[index].command)
         d(chatEmoteMap[index].keywords)
      end
   end

   for keyword in string.gmatch(text, "%S+") do
      -- Try to match phrases before single keywords.
      if Emotify.phraseMap[keyword] ~= nil then
         for phrase, alias in pairs(Emotify.phraseMap[keyword]) do
            if alias.channel[channel] ~= nil then
               if string.find(text, phrase, 1, true) then
                  return Emotify.PlayEmote(alias)
               end
            end
         end
      end

      -- If no phrases were found, a keyword match is tried.
      keyword = Emotify.filterText(keyword)
      if Emotify.keywordMap[keyword] ~= nil then
         alias = Emotify.keywordMap[keyword]
         if alias.channel[channel] ~= nil then
            return Emotify.PlayEmote(alias)
         end
      end
   end
end



--[[** Instantiate our AddOn! Yay!! ^-^ **]]--

-- I believe Wykkyd handles everything from here; Thank you!!
LWF3.REGISTER_FACTORY(
Emotify, false, true,
   function(self) Emotify:LoadSavedVariables(self) end,
   function(self) Emotify:LoadSettingsMenu(self) end,
   function(self) Emotify:Initialize(self) end,
   "chatEmoteGlobal", true
)

-- I am not sure what this does...
-- but Wykkyd does it, so I am doing it too!
chatEmote = Emotify

