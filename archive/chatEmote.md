
Basic Plan
==========
1. Construct the environment:
  - check emote map.
  - convert the emote map into an alias map.
  - know our events.
  - bind our event handler.
2. When a message is sent our event handler is triggered.
3. Continue if message channel is one of: local, emote, group, zone, yell.
4. Filter out everything except letters, numbers, and spaces.
5. Split the message by spaces and to get an array of words.
6. Check each word for an emote object.
7. When an emote object exists, check emote.events against current events.
8. When events match, execute the emote object.


Road map
========
* Add configuration section to specify:
  * New keywords
  * A corresponding emote
  * Under which events it is allowed to execute.


Events
======
* no_combat = 1    // Not engaged in combat.
* first_word = 2   // First word of sentence (after filtering).
* last_word = 4    // Last word of message (after filtering).

Events example
--------------
- no_combat = 1,
- first_word = 2,
- no_combat + first_word = 3,
- last_word = 4,
- no_combat + last_word  = 5,
- first_word + last_word = 6,
- no_combat + first_word + last_word = 7,


Abstract
========

Properties
----------
* constant no_combat = 1
* constant first_word = 2
* constant last_word = 4

* emote_map: array
  * 0 => {emote = lol; keywords = lol, haha; events = no_combat;}
  * 1 => {emote = dance; keywords = celebrate; events = no_combat + last_word;}

* alias_map: array
  * lol => &ref to 0,
  * haha => &ref to 0,
  * celebrate => &ref to 1,

Methods
-------
* get_alias_map(emote_map): array  // Convert emote_map into an alias_map.
* get_emote(alias): obj|false      // Check alias_map for alias, return object.
* get_events_int(events): int      // Corresponds with an event integer.
* get_events_array(int): array     // Get event strings from an event integer.
* chat_message_handler(): void     // Event handler for chat messages.
* filter_message(message): string  // Strips everything except letters, numbers, and spaces from a string.
* get_keywords(message): array     // Breaks a chat message down into a keywords array


